//
//  ViewController.swift
//  Pokedex2
//
//  Created by COTEMIG on 21/02/22.
//

import UIKit

class ViewController: UIViewController {

    var nomeDoUsuario: String?
    var inteiro: Int = 12
    var decimal : Double = 12.7
    var verdadeiroOuFalso: Bool = true
    
    func inicio() {
    
        guard let nomeDoUsuario = nomeDoUsuario else {
            // deu errado
            return
        }
        
        // deu certo
        
//        if let nome = nomeDoUsuario {
//            // de certo
//            funcaoComParametro(nome: nome, senha: 13)
//        } else {
//            // deu errado
//        }
        
        
        funcaoComParametro(nome: nomeDoUsuario, senha: 13)
    }
    
    func metodo() {
        print("Linha 1")
        print("Linha 2")
        print("Linha 3")
        return
            
        print("Linha 4")
        print("Linha 5")
    }
    
    func funcaoComParametro(nome: String, senha: Int) {
        print(nome)
        print(senha)
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var buttonHabilidades: UIButton!
    @IBOutlet weak var inputText: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        print("viewDidLoad - A tela foi carregada")
        
        buttonHabilidades.isEnabled = false
        metodo()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(clickNoLabel))
        titleLabel.isUserInteractionEnabled = true
        titleLabel.addGestureRecognizer(tapGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        print("viewWillAppear - A tela vai ser exibida")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        print("viewDidAppear - A tela foi exibida")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("viewDidAppear - A tela vai desaparecer")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("viewDidDisappear - A tela desapareceu")
    }
    
    @objc func clickNoLabel() {
        print("Clicou no label")
    }

    @IBAction func clickPokedex(_ sender: Any) {
        titleLabel.text = "Clicou no Pokedex"
        titleLabel.textColor = .cyan
        buttonHabilidades.isEnabled = true
    }
    
    @IBAction func clickMovimentos(_ sender: Any) {
        print("Clicou no Movimentos")
    }
    
    @IBAction func login(_ sender: Any) {
        if inputText.text == "wlad" {
            // abrir a segunda
            performSegue(withIdentifier: "LoginSegue", sender: nil)
        } else {
            print("Não permitido")
        }
    }
    
}

